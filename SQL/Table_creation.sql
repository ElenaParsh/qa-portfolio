--1) ������� ������� employees -id. serial, primary key, -employee_name. Varchar(50), not null
create table employees (
	id serial primary key,
	employee_name varchar(50) unique not null
);

-- 2) ��������� ������� employees 70 ��������
insert into employees (employee_name)
values ('Ivan_Ivanov'),
	   ('Petr_Petrov'),
	   ('Dmitry_Dmitriev'),
	   ('Kunskaya_Larisa'),
	   ('Davidivich_Geoigiy'),
	   ('Lapin_Eugeniy'),
	   ('Pavlov_Igor'),
	   ('Krasnovsky_Sergei'),
	   ('Danilova_Vera'),
	   ('Sodirenko_Maxim'),
	   ('Sidorova_Mariya'),
	   ('Drozdova_Anna'),
	   ('Grekova_Nadezhda'),
	   ('Sivenkov_Evgeniy'),
	   ('Smancer_Mariya'),
	   ('Lugovcov_Mihail'),
	   ('Kozlov_Nikolai'),
	   ('Vashkevich_Elizaveta'),
	   ('Britov_Alexei'),
	   ('Evmenov_Yuri'),
	   ('Solenkov_Gennadiy'),
	   ('Savitskaya_Vera'),
	   ('Lysenko_Viktoriya'),
	   ('Zarembo_Sviatoslav'),
	   ('Klys_Inna'),
	   ('Volochko_Aleksandr'),
	   ('Gordienko_Anatoly'),
	   ('Churkina_Yulia'),
	   ('Petrova_Galina'),
	   ('Gorodnickiy_Sviatoslav'),
	   ('Lipen_Eduard'),
	   ('Stankevich_Elena'),
	   ('Jakubenko_Viktor'),
	   ('Karpushenko_Andrei'),
	   ('Bobkova_Elizaveta'),
	   ('Burdo_Aleksei'),
	   ('Sakovec_Svetlana'),
	   ('Heifec_Ella'),
	   ('Ignatenko_Olga'),
	   ('Beliaev_Vladimir'),
	   ('Orlova_Anna'),
	   ('Kollontai_Maria'),
	   ('Gilevich_Angelina'),
	   ('Shkoda_Matvei'),
	   ('Levin_Boris'),
	   ('Butko_Ivan'),
	   ('Kovaleva_Evgenia'),
	   ('Leonovich_Sergei'),
	   ('Nikolaev_Igor'),
	   ('Cvetaeva_Marina'),
	   ('Pushkin_Aleksandr'),
	   ('Lermontov_Mihail'),
	   ('Kolas_Yakub'),
	   ('Dostoevsky_Fedor'),
	   ('Shopen_Frederic'),
	   ('Mayakovski_Vladimir'),
	   ('Gumilev_Nikolai'),
	   ('Marshak_Samuil'),
	   ('Shukshin_Vasiliy'),
	   ('Severyanin_Igor'),
	   ('Esenin_Sergei'),
	   ('Tolstoy_Lev'),
	   ('Ahmatova_Anna'),
	   ('Chehov_Anton'),
	   ('Turgenev_Ivan'),
	   ('Nekrasov_Nikolai'),
	   ('Ahmadulina_Bella'),
	   ('Rozhdestvensky_Robert'),
	   ('Nikolai_Rerih'),
	   ('Kuprin_Aleksandr');
	   
select * from employees;

-- 3)������� ������� salary -id. serial, primary key, - monthly_salary. int, not null
create table salary (
	id serial primary key,
	monthly_salary int not null
);

-- 4) ��������� ������� salary 16 ��������
insert into salary (monthly_salary) 
values (1000),
	   (1100),
	   (1200),
	   (1300),
	   (1400),
	   (1500),
	   (1600),
	   (1700),
	   (1800),
	   (1900),
	   (2000),
	   (2100),
	   (2200),
	   (2300),
	   (2400),
	   (2500);
	   
select * from salary;

--5) ������� ������� employee_salary -id. serial, primary key, -employee_id. int, not null, unique - salary_id. int, not null
 
create table employee_salary (
	id serial primary key,
	employee_id int not null unique,
	salary_id int not null
);

--6) ��������� ������� employee_salary 40 ��������: - � 10 ����� �� 40 �������� �������������� employee_id 

insert into employee_salary (employee_id, salary_id)
values (3, 7),
	   (1, 4),
	   (5, 9),
	   (40, 13),
	   (23, 4),
	   (11, 2),
	   (52, 10),
	   (15, 13),
	   (26, 4),
	   (16, 1),
	   (33, 7),
	   (20, 3),
	   (73, 16),
	   (30, 16),
	   (45, 14),
	   (80, 5),
	   (57, 12),
	   (90, 2),
	   (62, 15),
	   (70, 8),
	   (72, 1),
	   (68, 6),
	   (75, 4),
	   (64, 11),
	   (76, 8),
	   (49, 5),
	   (8, 8),
	   (99, 1),
	   (59, 16),
	   (22, 16),
	   (105, 3),
	   (36, 8),
	   (43, 5),
	   (86, 6),
	   (38, 4),
	   (28, 5),
	   (18, 1),
	   (13, 10),
	   (93, 8),
	   (12, 13);

 select * from employee_salary;

--7) ������� ������� roles -id. serial, primary key, -role_name. int, not null, unique
create table roles (
	id serial primary key,
	role_name int not null unique
);

select * from roles; 

--8) �������� ��� ������� role_name � int �� varchar(30)

alter table roles 
alter column role_name type varchar(30) using role_name::varchar(30);

--9) ��������� ������� roles 20 ��������

insert into roles (id, role_name)
values (default, 'Junior_Python_developer' ),
	   (default, 'Middle_Python_developer'),
	   (default, 'Senior_Python_developer'),
	   (default, 'Junior_Java_developer'),
	   (default, 'Middle_Java_developer'),
	   (default, 'Senior_Java_developer'),
	   (default, 'Junior_Manual_QA_Engineer'),
	   (default, 'Middle_Manual_QA_Engineer'),
	   (default, 'Senior_Manual_QA_Engineer'),
	   (default, 'Project_Manager'),
	   (default, 'Designer'),
	   (default, 'HR'),
	   (default, 'CEO'),
	   (default, 'Sales_Manager'),
	   (default, 'Junior_Automation_QA_Engineer'),
	   (default, 'Middle_Automation_QA_Engineer'),
	   (default, 'Senior_Automation_QA_Engineer');

select * from roles;

--10) ������� ������� roles_employee -id. serial primary key, - employee_id. int, not null, unique (������� ����
--��� ������� employees� ���� id) - role_id. int, not null (������� ���� ��� ������� roles, ���� id) 

create table roles_employee (
	id serial primary key,
	employee_id int not null unique,
	role_id int not null,
		foreign key (employee_id)
			references employees(id),
		foreign key (role_id)
			references roles(id)
);
	   
select * from roles_employee;

--��������� ������� roles_employee 40 �������� (id, employee_id, role_id)

insert into roles_employee (id, employee_id, role_id)
values (default, 7, 2),
	   (default, 20, 4),
	   (default, 3, 9),
	   (default, 5, 13),
	   (default, 23, 4),
	   (default, 11, 2),
	   (default, 10, 9),
	   (default, 22, 13),
	   (default, 21, 3),
	   (default, 34, 4),
	   (default, 6, 7),
	   (default, 13, 5),
	   (default, 15, 5),
	   (default, 17, 7),
	   (default, 19, 9),
	   (default, 25, 11),
	   (default, 29, 13),
	   (default, 27, 15),
	   (default, 40, 17),
	   (default, 31, 6),
	   (default, 38, 8),
	   (default, 33, 10),
	   (default, 36, 12),
	   (default, 48, 14),
	   (default, 46, 16),
	   (default, 50, 4),
	   (default, 44, 5),
	   (default, 52, 9),
	   (default, 42, 10),
	   (default, 55, 1),
	   (default, 57, 17),
	   (default, 59, 15),
	   (default, 67, 8),
	   (default, 65, 3),
	   (default, 63, 11),
	   (default, 8, 6),
	   (default, 1, 9),
	   (default, 69, 10),
	   (default, 70, 1),
	   (default, 61, 7);
	  

	 